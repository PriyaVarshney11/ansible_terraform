module "vpc" {
  source   = "./modules/vpc"
  vpc_cidr = var.cidr_vpc1
  vpc_tag  = var.vpc_tag1
}
module "subnet" {
  source            = "./modules/subnet"
  vpc_id            = module.vpc.vpc_id
  pub_cidr_block    = var.pub_cidr_block1
  pub_subnet_name   = var.pub_subnet_name1
  availability_zone = var.availability_zone2
}
module "igw" {
  source   = "./modules/igw"
  vpc_id   = module.vpc.vpc_id
  igw_name = var.igw_name1
}
module "route-table" {
  source      = "./modules/route-table"
  pub_rt_name = var.pub_rt_name1
  vpc_id      = module.vpc.vpc_id
  gateway_id  = module.igw.igw_id
  subnet_id   = module.subnet.subnet
}
module "security-group" {
  source      = "./modules/security-group"
  vpc_id      = module.vpc.vpc_id
  sg_name     = var.sg_name1
  web_ingress = var.web_ingress1
}
module "instance" {
  source            = "./modules/instance"
  instance_type     = var.instance_type1
  availability_zone = var.availability_zone3
  key_name          = var.key_name1
  pub_instance_name = var.pub_instance_name1
  ami               = var.ami1
  security_groups   = module.security-group.sg-output
  #vpc_id            = module.vpc.vpc_id
  subnet_id = module.subnet.subnet
}
