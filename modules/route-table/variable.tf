# Route table variables
variable "pub_rt_name" {
  description = "public route table name"
  type = string
  default = "kibana-route-table"
}

variable "vpc_id" {
  description = "subnet vpc name"
  type = string

}

variable "gateway_id" {
  description = "internet gateway name"
  type = string

}

variable "subnet_id" {
  description = "subnet name"
  type = string

}
