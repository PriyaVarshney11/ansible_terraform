# igw variables
variable "igw_name" {
  description = "igw name"
  type = string
  default = "kibana-igw"
}

variable "vpc_id" {
  description = "subnet vpc name"
  type = string
}
