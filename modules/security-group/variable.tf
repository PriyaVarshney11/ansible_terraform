variable "web_ingress" {
  type = map(object({
  port = number
  protocol = string
  cidr_blocks = list(string)
  }))
  default = {
    "port1" = {
      cidr_blocks = [ "0.0.0.0/0" ]
      port = 22
      protocol = "tcp"
    }
    "port2" = {
      cidr_blocks = [ "0.0.0.0/0" ]
      port = 5601
      protocol = "tcp"
    }
    "port3" = {
      cidr_blocks = [ "0.0.0.0/0" ]
      port = 9200
      protocol = "tcp"
    }
    }
}

variable "sg_name" {
  description = "name of security group"
  type = string
  default = "kibana-security-group"
}
variable "vpc_id" {
  description = "subnet vpc name"
  type = string
}
