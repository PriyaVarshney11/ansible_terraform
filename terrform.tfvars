cidr_vpc1 = "10.0.0.0/16"
vpc_tag1  = "kibana-vpc"

pub_cidr_block1  = "10.0.1.0/24"
pub_subnet_name1 = "kibana-subnet"

igw_name1 = "kibana-igw"

pub_rt_name1 = "kibana-route-table"

sg_name1 = "kibana-security-group"

ami1               = "ami-053b0d53c279acc90"
instance_type1     = "t2.medium"
key_name1          = "001key"
pub_instance_name1 = "kibana-tool"
availability_zone2 = "us-east-1a"
availability_zone3 = "us-east-1a"
